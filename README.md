“Thought leadership should be an entry point to a relationship. Thought leadership should intrigue, challenge, and inspire even people already familiar with a company. It should help start a relationship where none exists, and it should enhance existing relationships.”
– Daniel Rasmus

In our field, where relationships matter as much as the quality of the products we deliver, trust is key. 

Why do clients trust Mäd? Because we know our stuff, and we get it done. Simply put, we need a place where we showcase what we know, not only as a sales tool, but also by chance, what we know might also help others to win in their ventures. However, to showcase what we know is not enough. 

The house where we put our thoughts, that also has to look stunning. Otherwise our guests wouldn't stay and listen to what we have to say.

So here it is, the blueprint for that. A house for your thoughts and knowledge.

Happy writing.

Mäd